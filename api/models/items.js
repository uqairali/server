const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ItemsSchema = mongoose.Schema({
    name: {
        type: String,
        unique: 1,
    },
    price:{
        type:Number,
    },
    shortKey:{
        type:String,
        unique: 1,
    },
    count:{
        type:Number,
        default:0
    }
    
},
{ timestamps: true }
)

const Items = mongoose.model('item', ItemsSchema);

module.exports = Items;


