const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AddressSchema = mongoose.Schema({
    address: {
        type: String,
        unique: 1,
    },
    names: [{
        type: Schema.Types.ObjectId,
        ref: 'account'
    }]

}, {
    timestamps: true
})

const Address = mongoose.model('addres', AddressSchema);

module.exports = Address;