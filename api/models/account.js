const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AcccountSchema = mongoose.Schema({
    name: {
        type: String,
    },
    contactNumber: {
        type: String,
    },
    address: {
        type: Schema.Types.ObjectId,
        ref: 'addres'
    },
    ammount: {
        type: Number
    },
    accountId: {
        type: String
    },
    accountLimit: {
        type: Number
    },
    recipts: [{
        type: Schema.Types.ObjectId,
        ref: 'recipt'
    }],
    pay: [{
        type: Schema.Types.ObjectId,
        ref: 'pay'
    }]


}, {
    timestamps: true
})

const Account = mongoose.model('account', AcccountSchema);

module.exports = Account;