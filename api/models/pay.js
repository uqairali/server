const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const PaySchema = mongoose.Schema({
    name: {
        type: Schema.Types.ObjectId,
        ref: 'account'
    },
    ammount: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true
    },


}, {
    timestamps: true
})

const Pay = mongoose.model('pay', PaySchema);

module.exports = Pay;