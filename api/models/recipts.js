const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const reciptSchema = mongoose.Schema({
    clientName: {
        type: Schema.Types.ObjectId,
        ref: 'account'
    },
    totalAmmount: {
        type: Number
    },
    serialNumber: {
        type: Number
    },
    date: {
        type: Date,
        required: true
    },
    recipt: {
        type: Array
    },

}, {
    timestamps: true
})

const Recipt = mongoose.model('recipt', reciptSchema);

module.exports = Recipt;