const Account = require('../models/account')
const Address = require('../models/address')
exports.newAccount = async (req, res) => {
    var pushVal = "names"
    try {
        const account = new Account(req.body)
        var doc = await account.save()
        await Address.findByIdAndUpdate({ _id: req.body.address }, { $push: { [pushVal]: doc._id } }, { new: true })
        res.status(200).json(doc)

    } catch (err) {
        res.status(400).json(err)
    }
}
exports.getAllAccounts = async (req, res) => {
    try {
        var doc = await Account.find().populate([
           { path: 'address',
            match: {
                //active: true
            }},
            {
                path: 'pay',
                match: {
                    //active: true
                }}

            ]).sort({createdAt:-1})
        res.status(200).json(doc)

    } catch (err) {
        res.status(400).json(err)
    }
}
exports.updateAccount = async (req, res) => {
    var pushVal = "names"
    var popFiled = "names"
    try {
        await Address.findOneAndUpdate({ _id: req.params.previusAddress }, { $pull: { [popFiled]: req.params.id } })
        var doc = await Account.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
        await Address.findByIdAndUpdate({ _id: req.body.address }, { $push: { [pushVal]: req.params.id } }, { new: true })
        res.status(200).json(doc)

    } catch (err) {
        res.status(400).json(err)
    }
}

exports.deleteAccount=async(req,res)=>{
    try{
        var popFiled="names"
     const doc=await Account.findOneAndDelete({ _id: req.params.id })
     await Address.findOneAndUpdate({ _id: req.params.addressId }, { $pull: { [popFiled]: req.params.id } })
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }
  exports.getByAccountKey=async(req,res)=>{
    try{
     const doc=await Account.findOne({ accountId: req.params.key })
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }
  exports.getByAccountById=async(req,res)=>{
    try{
     const doc=await Account.findOne({ _id: req.params.id }).populate({
        path: 'address',
        match: {
            //active: true
        },

    })
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }
