const AccountId = require('../models/accountId');

exports.incrementAccountId = async (req, res) => {
    try {

        var data = await AccountId.findOneAndUpdate({ _id: req.params.id }, { $inc: { accountId: 1 } }, { new: true })
        res.status(200).json(data)
    }
    catch (err) {
        res.status(400).json(err)
    }

}

exports.getAccouontId = async (req, res) => {
    try {
        const doc = await AccountId.find()
        if (!doc.length) {
            const accountId = new AccountId({ accountId: 1 })
            var newDoc = await accountId.save()
            res.status(200).json(newDoc)
        } else
            res.status(200).json(doc)
    } catch (err) {
        res.status(400).json(err)
    }
}
