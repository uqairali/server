const Pay = require('../models/pay')
const { getDateFromParam } = require('../../utils/helper')
const Account = require('../models/account')
exports.newPay = async (req, res) => {
    try {
        let date = req.params.date ? getDateFromParam(req.params.date).toDateString() : new Date().toDateString(); //new Date('01/26/2020')
        req.body.date = date
        const pushVal = "pay"
        const pay = new Pay(req.body)
        var doc = await pay.save()
        await Account.findByIdAndUpdate({ _id: req.body.name }, {
            $push: { [pushVal]: doc._id },
        }, { new: true })
        res.status(200).json(doc)

    } catch (err) {
        res.status(400).json(err)
    }
}
exports.updatePay = async (req, res) => {
    try {
        let date = req.params.date ? getDateFromParam(req.params.date).toDateString() : new Date().toDateString(); //new Date('01/26/2020')
        req.body.date = date
        const doc = await Pay.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
        res.status(200).json(doc)
    } catch (err) {
        res.status(400).json(err)
    }
}

exports.getAllPayes = async (req, res) => {
    try {
        const doc = await Pay.find().populate({
            path: 'name',

        })
        res.status(200).json(doc)
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.deletePays = async (req, res) => {
    try {
        const doc = await Pay.findByIdAndDelete({_id:req.params.id})
        res.status(200).json(doc)
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.getPayesByName = async (req, res) => {
    try {
        const doc = await Pay.find({name:req.params.id}).populate({
            path: 'name',

        })
        res.status(200).json(doc)
    } catch (err) {
        res.status(400).json(err)
    }
}

