const Address =require('../models/address')
exports.newAddress = async (req, res) => {
    try {
            const address = new Address(req.body)
            var doc = await address.save()
            res.status(200).json(doc)
       
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.updateaddress=async(req,res)=>{
    try{
     const doc=await Address.findByIdAndUpdate({_id:req.params.id},req.body,{new:true})
    res.status(200).json(doc)
    }catch(err){
        res.status(400).json(err)
    }
}
exports.findAllAddress=async(req,res)=>{
    console.log("alladdress")
    try{
     const doc=await Address.find().populate({
        path: 'names',
        match: {
          //active: true
        },
        
      })
    res.status(200).json(doc)
    }catch(err){
        res.status(400).json(err)
    }
}

exports.deleteAddress=async(req,res)=>{
    try{
     const doc=await Address.findOneAndDelete({ _id: req.params.id })
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }