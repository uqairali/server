const ReciptSN = require('../models/reciptSN');

exports.incrementSN=async(req,res)=>{
    try{

        var data=await ReciptSN.findOneAndUpdate({_id:req.params.id}, { $inc: { serialNumber: 1 } } )
        res.status(200).json(data)
    }
    catch(err){
        res.status(400).json(err)
    }

}

exports.getReciptSN=async(req,res)=>{
  try{
   const doc=await ReciptSN.find()
   if(doc.length>=1){
    res.status(200).json(doc[0])
   }else{
    const reciptSN=new ReciptSN({serialNumber:1})
    const data= await reciptSN.save()
    res.status(200).json(data)
   }
  }catch(err){
    res.status(400).json(err)
  }
}
