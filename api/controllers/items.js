const Items =require('../models/items')
exports.newItems = async (req, res) => {
    try {
            const items = new Items(req.body)
            var doc = await items.save()
            res.status(200).json(doc)
       
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.getAllItems = async (req, res) => {
    try {
            var doc = await Items.find().sort({createdAt:-1})
            res.status(200).json(doc)
       
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.updateItems = async (req, res) => {
    try {
            var doc = await Items.findByIdAndUpdate({_id:req.params.id},req.body)
            res.status(200).json(doc)
       
    } catch (err) {
    res.status(400).json(err)
    }
}
exports.deleteItems = async (req, res) => {
    try {
            var doc = await Items.findByIdAndDelete({_id:req.params.id})
            res.status(200).json(doc)
       
    } catch (err) {
    res.status(400).json(err)
    }
}
