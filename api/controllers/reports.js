
const Account =require('../models/account')
exports.generalReport=async(req,res)=>{
    try{ 
       var accountData=await Account.find();

       var result={
           totalAmmount:0,
           totalAccounts:0
       };
       if(accountData.length>=1){
        accountData.map(itm=>{
            result.totalAmmount+=itm.ammount
        })
        result.totalAccounts+=accountData.length
    }
    res.status(200).json(result)
    }catch(err){
        res.status(400).json(err)
    }
}