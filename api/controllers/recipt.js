const Recipt = require('../models/recipts');
const { getDateFromParam,startAndEndTime } = require('../../utils/helper')
const Account =require('../models/account')

exports.saveRecipt = async(req, res) => {
  let date = new Date(req.params.date)
  req.body.date=date
  try{
    const pushVal="recipts"
    const totalAmmount=+req.body.totalAmmount
    const recipt = new Recipt(req.body)
    const doc=await recipt.save()
    await Account.findByIdAndUpdate({ _id: req.body.clientName }, { 
      $push: { [pushVal]: doc._id },
       $inc:{ammount:totalAmmount},
    }, { new: true })
    res.status(200).json(doc)
  }
  catch(err){
    res.status(400).json(err)
  }

}

exports.getRecipt = async (req, res) => {
  const date=new Date(req.params?.date)
  if(!date){
    res.status(401).send({ message: "Please provide valid date e.g. 01-24-2020 " })
  }
  const {startOfTheDay,endOfTheDay}=startAndEndTime(date)
  try {
    const doc=await Recipt.find({
      date: {
        $gte: startOfTheDay,
        $lte: endOfTheDay
      }
    }).populate({
      path:'clientName',
      populate:{
        path:"address"
      }
    }).sort({ createdAt : -1 })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}

exports.getReciptByDateRange = async (req, res) => {
  const {startDate,endDate,userId}=req.body
    
  const {startOfTheDay}=startAndEndTime(new Date(startDate))
  const {endOfTheDay}=startAndEndTime(new Date(endDate))
  try {
    const doc=await Recipt.find({
      date: {
        $gte: startOfTheDay,
        $lte: endOfTheDay
      },
      clientName:userId
    }).populate({
      path:'clientName',
      populate:{
        path:"address"
      }
    }).sort({ createdAt : -1 })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}


exports.updateRecipt = async (req, res) => {
  let date = req.params.date ? getDateFromParam(req.params.date).toDateString() : new Date().toDateString(); //new Date('01/26/2020')
  req.body.date=date
  var total=+req.params.total;
  try {
    const doc = await Recipt.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
    await Account.findByIdAndUpdate({ _id: doc.clientName }, { 
       $inc:{ammount:-total},
    })
    await Account.findByIdAndUpdate({ _id: doc.clientName }, { 
      $inc:{ammount:doc.totalAmmount},
   })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}

exports.deleteRecipt = async (req, res) => {
  try {
    const pullhVal="recipts"
    const total=+req.params.total
    const doc = await Recipt.findOneAndDelete({ _id: req.params.id })
    await Account.findByIdAndUpdate({ _id: req.params.accountId },
       { 
         $pull: { [pullhVal]: req.params.id },
         $inc:{ammount:-total}, 
        
        }, { new: true })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.getReciptById=async(req,res)=>{
  try{
    const doc=await Recipt.find({_id:req.params.id})
    res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}
exports.getReciptByName=async(req,res)=>{
  try{
     const docs=await Recipt.find({clientName:req.params.name})
     .sort({createdAt:-1})
     .populate({
      path:'clientName',
      populate:{
        path:"address"
      }
    });
     res.status(200).json(docs)
  }catch(err){
    res.status(400).json(err)
  }
}
exports.getReciptBySerialNumber=async(req,res)=>{
  var serial=+req.params.serialnumber
  try{
     const docs=await Recipt.findOne({serialNumber:serial})
     res.status(200).json(docs)
  }catch(err){
    res.status(400).json(err)
  }
}
