const express = require('express');
const apiRoutes = express.Router();
const auth = require('../config/auth')
const AgentController = require('./controllers/agent')
const AddressController = require('./controllers/address')
const AccountIdController = require('./controllers/accountId')
const AccountController = require('./controllers/account')
const ItemsController = require('./controllers/items')
const SerialNumberController=require('./controllers/reciptSN')
const ReciptController=require('./controllers/recipt')
const PayController=require('./controllers/pay')
const repostController=require('./controllers/reports')
apiRoutes.post('/auth/login', auth.optional, AgentController.login)
apiRoutes.post('/auth/register', auth.optional, AgentController.register)

apiRoutes.post('/newAddress', AddressController.newAddress)
apiRoutes.get('/allAddreses', AddressController.findAllAddress)
apiRoutes.put('/editAddress/:id', AddressController.updateaddress)
apiRoutes.delete('/deleteAddress/:id', AddressController.deleteAddress)

apiRoutes.get('/getAccountId', AccountIdController.getAccouontId)
apiRoutes.get('/incrementAccountId/:id', AccountIdController.incrementAccountId)

apiRoutes.post('/newAccount', AccountController.newAccount)
apiRoutes.get('/getAllAccounts', AccountController.getAllAccounts)
apiRoutes.put('/updateAccounts/:id/:previusAddress', AccountController.updateAccount)
apiRoutes.delete('/deleteAccounts/:id/:addressId', AccountController.deleteAccount)
apiRoutes.get('/getAccountByKey/:key', AccountController.getByAccountKey)
apiRoutes.get('/getAccountById/:id', AccountController.getByAccountById)

apiRoutes.post('/newItem', ItemsController.newItems)
apiRoutes.get('/getAllItems', ItemsController.getAllItems)
apiRoutes.put('/updateItem/:id', ItemsController.updateItems)
apiRoutes.delete('/deleteItem/:id', ItemsController.deleteItems)

apiRoutes.get('/getReciptSerialNumber', SerialNumberController.getReciptSN)
apiRoutes.put('/updateReciptSN/:id', SerialNumberController.incrementSN)

apiRoutes.post('/newRecipt/:date', ReciptController.saveRecipt)
apiRoutes.get('/getReciptByDate/:date', ReciptController.getRecipt)
apiRoutes.post('/getReciptByDateRange', ReciptController.getReciptByDateRange)

apiRoutes.get('/getReciptByName/:name', ReciptController.getReciptByName)
apiRoutes.delete('/deleteRecipt/:id/:accountId/:total', ReciptController.deleteRecipt)
apiRoutes.get('/getReciptById/:id', ReciptController.getReciptById)
apiRoutes.put('/updateRecipt/:id/:date/:total', ReciptController.updateRecipt)

apiRoutes.post('/newPay/:date', PayController.newPay)
apiRoutes.get('/payes/', PayController.getAllPayes)
apiRoutes.put('/updatePay/:id/:date', PayController.updatePay)
apiRoutes.get('/getPayesByName/:id', PayController.getPayesByName)
apiRoutes.delete('/deletePays/:id', PayController.deletePays)

apiRoutes.get('/allReport', repostController.generalReport)

apiRoutes.get('/test', (req, res) => {
    res.status(200).json("server is running")
})

module.exports = apiRoutes;