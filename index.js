const express = require('express');

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');

require('./api/models/agents')
require('./config/passport')
const apiRoutes = require("./api/api");
const app = express();
const mongoose = require('mongoose');
require('dotenv').config();

const Recipt = require('./api/models/recipts');
const Pay = require('./api/models/pay');


//mongoose connections
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DATABASE, ((err,db) => {
    if (err) {
        console.log(process.env.DATABASE)
        console.log("mongodb connection failed!!!!")
    } else {
        console.log("mongodb connected success")
        require('./api/controllers/backup')

    }
}))

app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

app.use(cookieParser());
app.use(cors());
app.use('/api', apiRoutes);

app.get("/allStringDateToDate",async(req,res)=>{
     const allReceipts=await Recipt.find()
     for(var i=0;i<allReceipts.length;i++){
        await Recipt.findByIdAndUpdate({_id:allReceipts[i]._id},{date:new Date(allReceipts[i].date)})
        console.log("updated--",i)
     }
})
app.get("/allStringDateToDateForPayments",async(req,res)=>{
    const allPay=await Pay.find()
    for(var i=0;i<allPay.length;i++){
       await Pay.findByIdAndUpdate({_id:allPay[i]._id},{date:new Date(allPay[i].date)})
       console.log("updated--",i)
    }
})
const port = process.env.PORT || 3002;
app.listen(port, () => {
    console.log(`server is running on port ${port}`)
})