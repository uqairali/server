
const moment =require("moment");

exports.deleteWallpapersByCategory=(findVal,doc)=>{
  var newDoc=[];
  doc.map(item=>{
    var filteredAry = item.category.filter(function(e) { return e !== findVal })
     newDoc.push({category:filteredAry})
  })
  return newDoc
}
exports.getDateFromParam= (date)=>{
  return new Date(date.replace(/-/g,' '));
}


exports.startAndEndTime=(date)=>{
  let startOfTheDay=moment(date).startOf('day')
const endOfTheDay=moment(date).endOf('day')
return {startOfTheDay:startOfTheDay.format('YYYY-MM-DDTHH:mm:ss'),endOfTheDay:endOfTheDay.format('YYYY-MM-DDTHH:mm:ss')}
}